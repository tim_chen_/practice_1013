require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user_params = {
      first_name: "test_first_name",
      last_name: "test_last_name",
      age: 20,
      gender: "male"
    }
  end

  describe '#first_name' do
    it 'is not valid without a last name' do
      @user_params.delete(:first_name)
      user = User.new(@user_params)
      expect(user).to_not be_valid
    end
  end

  describe '#last_name' do
    it 'is not valid without a first name' do
      @user_params.delete(:last_name)
      user = User.new(@user_params)
      expect(user).to_not be_valid
    end
  end

  describe '#gender' do
    it 'is valid when gender is male' do
      @user_params[:gender] = "male"
      user = User.new(@user_params)
      expect(user).to be_valid
    end

    it 'is valid when gender is female' do
      @user_params[:gender] = "female"
      user = User.new(@user_params)
      expect(user).to be_valid
    end

    it 'is valid when gender is others' do
      @user_params[:gender] = "others"
      user = User.new(@user_params)
      expect(user).to be_valid
    end

    it 'is not valid with illegal gender' do
      @user_params[:gender] = "illegal gender"
      user = User.new(@user_params)
      expect(user).to_not be_valid
    end
  end

  describe '#age' do
    it 'is valid with numerical age' do
      @user_params[:age] = 20
      user = User.new(@user_params)
      expect(user).to be_valid
    end

    it 'is not valid with non numerical age' do
      @user_params[:age] = "invalid_age"
      user = User.new(@user_params)
      expect(user).to_not be_valid
    end
  end
end
