require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  before(:all) do
    @user = User.create(last_name: "tim", first_name: "chen",
      gender: "male", age: 10)
  end

  describe "#index" do
    it 'should return 200 status code' do
      get :index, format: :json
      expect(response).to be_success
    end
  end

  describe "#show" do
    it 'should return 200 status code' do
      get :show, id: @user.id, format: :json
      expect(response).to be_success
    end
  end

  describe "#new" do
    it 'should return 200 status code' do
      get :new
      expect(response).to be_success
    end
  end

  describe "#edit" do
    it 'should return 200 status code' do
      get :edit, id: @user.id
      expect(response).to be_success
    end
  end

  describe "#create" do
    it 'should return 200 status code' do
      params = {
        user: {
          first_name: "test",
          last_name: "test",
          gender: "male",
          age: 10
        },
        format: :json
      }

      post :create, params
      expect(response).to be_success
    end
  end

  describe "#update" do
    it 'should return 200 status code' do
      params = {
        id: @user.id,
        user: {
          first_name: "new_first_name"
        },
        format: :json
      }

      put :update, params
      expect(response).to be_success
    end
  end

  describe "#destroy" do
    it 'should return 200 status code' do
      delete :destroy, id: @user.id, format: :json
      expect(response).to be_success
    end
  end
end
