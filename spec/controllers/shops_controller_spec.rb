require 'rails_helper'

RSpec.describe ShopsController, type: :controller do
  before(:all) do
    @user = User.create(last_name: "tim", first_name: "chen",
      gender: "male", age: 10)
    @shop = Shop.create(name: "test_shop")
    @user.shop = @shop
    @user_without_shop = User.create(last_name: "tim", first_name: "chen",
      gender: "male", age: 10)
  end 

  describe "#show" do
    it 'shold return 200 status code' do
      get :show, user_id: @user.id, format: :json
      expect(response).to be_success
    end
  end

  describe "#create" do
    it 'shold return 200 status code' do
      params = {
        user_id: @user_without_shop.id,
        shop: {
          name: "new_shop"
        },
        format: :json
      }

      post :create, params
      expect(response).to be_success
    end
  end

  describe "#update" do
    it 'shold return 200 status code' do
      params = {
        user_id: @user.id,
        shop: {
          name: "updated_shop"
        },
        format: :json
      }
      put :update, params
      expect(response).to be_success
    end
  end

  describe "#destroy" do
    it 'shold return 200 status code' do
      delete :destroy, user_id: @user.id, format: :json
      expect(response).to be_success
    end
  end
end
