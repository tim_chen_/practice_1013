app.directive('ageDirective', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, mCtrl) {
      function ageValidation(value) {
        if (typeof(value) === "number" && value > 0) {
          mCtrl.$setValidity("posNum", true);
        } else {
          mCtrl.$setValidity("posNum", false);
        }
        return value
      }
      mCtrl.$parsers.push(ageValidation);
    }
  };
});

app.directive('ngUser', function(){
  return {
    templateUrl: '/user_edit'
  }
})

app.factory('UserService', function($http, $q, $window) {
  return {
    getUser: function(){
      return $http({method: 'GET', url: '/users.json'})
        .then(function(response) {
          return response.data
        }, function(response) {
          return $q.reject(response.data)
        });
    },
    updateUser: function(newUserInfo, userId){
      return $http(
        {
          method: 'PUT',
          url: `/users/${userId}.json`,
          data: {
            first_name: newUserInfo.firstName,
            last_name: newUserInfo.lastName,
            age: newUserInfo.age,
            gender: newUserInfo.gender
          }
        }).
        then(function(response) {
          // $window.location.href = `/users/${userId}`
        })
    },
    deleteUser: function(userId){
      return $http(
        {
          method: 'DELETE',
          url: `/users/${userId}`
        }
      )
    },
    createUser: function(newUserInfo){
      return $http(
        {
          method: 'POST',
          url: '/users',
          data: {
            first_name: newUserInfo.firstName,
            last_name: newUserInfo.lastName,
            age: newUserInfo.age,
            gender: newUserInfo.gender
          }
        }
      )
    }
  }
});

app.controller('UsersCtrl', ['$scope', '$http', '$window', 'UserService', function($scope, $http, $window, UserService) {
  // $scope.data = UserService.getUser().
  //   then(function(response){
  //       $scope.data = response;
  //       angular.forEach($scope.data, function(value, key) {
  //         value.display = false
  //       })
  //     }
  //   )

  $scope.getUser = function(){
    UserService.getUser().
    then(function(response){
        $scope.data = response;
        angular.forEach($scope.data, function(value, key) {
          value.display = false
        })
      }
    )
  }

  $scope.updateUser = function(updateUserInfo, user, index) {
    UserService.updateUser(updateUserInfo, user.id.$oid).
      then(function(response){
        mapping = {
          "firstName": "first_name",
          "lastName": "last_name",
          "age": "age",
          "gender": "gender"
        }
        angular.forEach(updateUserInfo, function(value, key){
          $scope.data[index][mapping[key]] = value
        })
        $scope.data[index][updateUserInfo]
      })
  }

  $scope.deleteUser = function(user) {
    UserService.deleteUser(user.id.$oid).
      then(function(response){
        $scope.getUser();
      }, function(response){
        $scope.getUser();
      })

  }

  $scope.createUser = function(newUserInfo) {
    UserService.createUser(newUserInfo).
      then(function(response){
        $scope.getUser();
      })
  }

  $scope.isVisible = false;
  $scope.showHide = function(user) {
    user.display = ! user.display;
  }

}]);
