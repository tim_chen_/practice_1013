class User
    include Mongoid::Document
    include Mongoid::Timestamps

    has_one :shop

    field :first_name, type: String
    field :last_name, type: String
    field :gender, type: String
    field :age, type: Integer
    field :address, type: Hash

    validates :first_name, :last_name, presence: true
    validates :gender, inclusion: { in: %w(male female others) }
    validates :age, numericality: { only_integer: true, greater_than: 0 }
end
