class ShopsController < ApplicationController
  before_action :user, only: [:create]
  before_action :set_shop, only: [:show, :update, :destroy]
  skip_before_action :verify_authenticity_token

  # GET /users/:user_id/shop
  # GET /users/:user_id/shop.json
  def show
  end

  # POST /users/:user_id/shop
  # POST /users/:user_id/shop.json
  def create
    @shop = Shop.new(shop_params)

    respond_to do |format|
      if @user and @user.shop.nil? and @shop.save
        @user.shop = @shop
        format.json { render :show, status: :ok, location: user_shop_path(@shop) }
      else
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/:user_id/shop
  # PATCH/PUT /users/:user_id/shop
  def update
    respond_to do |format|
      if @shop and @shop.update(shop_params)
        format.json { render :show, status: :ok, location: user_shop_path(@shop) }
      else
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/:user_id/shop
  # DELETE /users/:user_id/shop.json
  def destroy
    @shop.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop
      @shop = User.find(params[:user_id]).shop
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shop_params
      params.require(:shop).permit(:name)
    end

    def user
      @user = User.find(params[:user_id])
    end
end
