json.extract! shop, :id, :name, :created_at, :updated_at
json.url user_shop_url(shop, format: :json)
